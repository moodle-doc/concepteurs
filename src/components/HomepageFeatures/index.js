import React from 'react';
import clsx from 'clsx';
import Heading from '@theme/Heading';
import styles from './styles.module.css';
import PresentationSVG from '@site/static/img/team_up.svg';
import DocumentationSVG from '@site/static/img/completed_tasks.svg';
import EcosystemeSVG from '@site/static/img/community.svg';
import EquipesSVG from '@site/static/img/equipes.svg';
import ConceptionsSVG from '@site/static/img/conceptions.svg';

const FeatureList = [];

export default function HomepageFeatures() {
  return (
    <section className='features_src-components-HomepageFeatures-styles-module homepage'>
      <div className="container">
        <div className="row">
          <div className={clsx('col col--8')}>
            <div className={clsx('container padding-horiz--md')}>
            <div className={clsx('row tuile')}>
              <div className={clsx('col col--9')}>
                <h3 class="homepage">Présentation</h3>
                <p className={clsx('text--justify')}>Découvrez une plateforme dédiée à la création de parcours d'apprentissages. Elle offre des outils pour concevoir en équipes, partager et enrichir vos contenus. Elle permet de fédérer une communauté d'enseignants et de formateurs concepteur de parcours, pour répondre aux besoins des apprenants.</p>
              </div>
              <div className={clsx('col col--3')}>
                <PresentationSVG className="homeSVG"/>
              </div>          
            </div>
            <div className={clsx('row tuile')}>
            <div className={clsx('col col--3')}>
            <DocumentationSVG className="homeSVG"/>
              </div> 
              <div className={clsx('col col--9')}>
                <h3 class="homepage">La documentation</h3>
                <p className={clsx('text--justify')}>Cette documentation complète vous guide pas à pas dans l’utilisation de la plateforme. Découvrez comment mettre en place les outils pour votre équipe. Puis créez, partagez et gérez vos conceptions grâce à des tutoriels clairs et des exemples pratiques.</p>
              </div>         
            </div>
            <div className={clsx('row tuile')}>
              <div className={clsx('col col--9')}>
                <h3 class="homepage">Un écosystème</h3>
                <p className={clsx('text--justify')}> La plateforme Réseau des concepteurs permet de concevoir et de partager des parcours pédagogiques Éléa pour les élèves et des parcours de formation Magistère pour les adultes. Ces conceptions viendront alimenter les catalogues de ces deux platefomes.</p>
              </div>
              <div className={clsx('col col--3')}>
              <EcosystemeSVG className="homeSVG"/>
              </div>          
            </div>
            </div>

            <div className={clsx('row vignettes-container')}>
              <div class="lancement">
                <div class="card__body">
                    <EquipesSVG className="homeSVG"/>
                </div>
                <div class="card__footer">
                  <a href='docs/category/equipes'><button class="button button--primary button--block">Comment gérer<br/> ses équipes</button></a>
                </div>
              </div>
              <div class="lancement">
                <div class="card__body">
                    <ConceptionsSVG className="homeSVG"/>
                </div>
                <div class="card__footer">
                <a href='docs/category/conceptions'><button class="button button--primary button--block">Comment gérer<br/> ses conceptions</button></a>
                </div>
              </div>
            </div>           


            </div>
        <div className={clsx('marge col col--4')}>
          <div class='tuile'>
            <h4 class="homepage">Pour se connecter, sélectionner votre profil :</h4>
            <p className={clsx('cadre')}><img src="https://test-concepteurs.apps.education.fr/theme/image.php/rdc/theme/1731639620/login-school" class="p-1"/>
              <span class="p-1">Enseignement scolaire :</span>  Pour tous les personnels du Ministère de l'Éducation nationale.
            </p>
            <p className={clsx('cadre')}><img src="https://test-concepteurs.apps.education.fr/theme/image.php/rdc/theme/1731639620/login-high-school" class="p-1"/>
              <span class="p-1">Enseignement supérieur :</span> Pour les étudiants et tous les personnels du Ministère de l'Enseignement supérieur et de la Recherche.
            </p>
            <p className={clsx('cadre')}><img src="https://test-concepteurs.apps.education.fr/theme/image.php/rdc/theme/1731639620/login-hub" class="p-1"/>
              <span class="p-1">Hub partenaires :</span> Pour les personnels de l'enseignement agricole, des établissements français à l'étranger, des partenaires institutionnels.
            </p>
            <p className={clsx('cadre')}><img src="https://test-concepteurs.apps.education.fr/theme/image.php/rdc/theme/1731639620/login-other" class="p-1"/>
              <span class="p-1">Autre compte :</span>Pour les invités
            </p>
          </div>
        </div>
        </div>
      </div>

    </section>
  );
}
