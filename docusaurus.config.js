// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Documentation pour l\'utilisation du Réseau des concepteurs',
  tagline: 'Créer, partager, faire vivre des parcours d\'apprentissages',
  favicon: '/img/concepteurs.ico',

  // Set the production url of your site here
  url: 'https://moodle-doc.forge.apps.education.fr/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/concepteurs/',


  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
         },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: '/img/concepteurs_logo.svg',
      navbar: {
        title: '',
        logo: {
          alt: 'logo de Concepteurs',
          src: '/img/concepteurs_logo.svg',
        },
        items: [
          {
            type: 'docSidebar',
            sidebarId: 'SidebarEquipes',
            position: 'left',
            label: 'Équipes',
          },
          {
            type: 'docSidebar',
            sidebarId: 'SidebarConceptions',
            position: 'left',
            label: 'Conceptions',
          },
          {to: 'docs/Signalement', label: 'Signalement', position: 'left'},
          {label: 'Documentation Éléa',href: 'https://moodle-doc.forge.apps.education.fr/elea',position: 'right',},
          {label: 'Documentation Magistère',href: 'https://moodle-doc.forge.apps.education.fr/magistere',position: 'right',},
        ],

      },
      algolia: {
        // L'ID de l'application fourni par Algolia
        appId: '1FXXSM1CUL',
  
        // Clé d'API publique : il est possible de la committer en toute sécurité
        apiKey: 'a17eb0c3fd50581168faf475869511e8',
  
        indexName: 'moodle-doc-ecosysteme',
  
        // Facultatif : voir la section doc ci-dessous
        contextualSearch: true,

        // Facultatif : paramètres de recherche de Algolia
        searchParameters: {},
  
        // Facultatif : chemin pour la page de recherche qui est activée par défaut (`false` pour le désactiver)
        searchPagePath: 'search',
  
        //... autres paramètres de Algolia
      },
     footer: {
        style: 'dark',
        links: [{label:'Accéder au Réseau des concepteurs', href:'https://concepteurs.apps.education.fr', position:'center' }],
        copyright: `Équipes nationales Éléa et Magistère, Ministère de l'Éducation nationale`,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
    }),

  plugins: [
    [
      '@docusaurus/plugin-ideal-image',
      {
        quality: 70,
        max: 1030, // max resized image's size.
        min: 640, // min resized image's size. if original is lower, use that size.
        steps: 2, // the max number of images generated between min and max (inclusive)
        disableInDev: false,
      },
    ],
  ],
};

export default config;
